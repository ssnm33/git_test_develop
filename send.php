<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>mail test</title>
  <link rel="stylesheet" href="css/normalize.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/mail.css">
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script type="text/javascript" src="js/script_mail.js"></script>
</head>
<body>
  <div id="is_loaded" class="l_wrapper header_first">
    <header class="">
      <nav>
        <h1>mail test</h1>
      </nav>
    </header>
    <section>
      <form class="mail_box" action="http://localhost:8888/00test/mail_get.php" method="post">
        <dl>
        <dt>名前</dt>
        <dd><input name="name" id="name" type="text" size="50" /></dd>
        <dt>性別</dt>
        <dd>
        <input name="gender" id="man" type="radio" value="男性" /><label for="man">男性</label><input name="gender" id="woman" type="radio" value="女性" /><label for="woman">女性</label>
        </dd>
        <dt>血液型</dt>
        <dd>
        <select name="blood" id="blood">
        <option value="A型">A型</option>
        <option value="B型">B型</option>
        <option value="O型">O型</option>
        <option value="AB型">AB型</option>
        </select>
        </dd>
        <dt>郵便番号</dt>
        <dd><input name="yubin" id="yubin" type="text" size="50" /></dd>

        <dt>メールアドレス（送信先）</dt>
        <dd><input name="mail" id="mail" type="text" size="50" maxlength="255" /></dd>

        <dt>件名</dt>
        <dd><input name="sub" id="sub" type="text" size="50" maxlength="255" /></dd>
        <dt>内容</dt>
        <dd><textarea name="naiyou" id="naiyou" cols="50" rows="10"></textarea></dd>
        </dl>
        <input type="submit" value="送信する" />
      </form>
    </section>
  </div><!-- //l_wrapper -->
</body>
</html>
