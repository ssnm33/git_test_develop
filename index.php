<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>include test</title>
  <link rel="stylesheet" href="css/normalize.css">
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/style.css">
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script type="text/javascript" src="js/script.js"></script>
</head>
<body>
  <div id="is_loaded" class="l_wrapper header_first">
    <header class="">
      <!--インクルード virtual="/header.html"-->
      <nav>
        <h1>test</h1>
        <!-- <i class="fa fa-instagram" aria-hidden="true"></i> -->
      </nav>

      <div class="icon_box">
        <i class="fa fa-youtube-square font_large" aria-hidden="true"></i>
      </div>

    </header>
    <section>
      <div class="content">
        <img src="images/img01.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img02.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img03.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img04.jpg" alt="">
      </div>
      <div class="content">
        <img src="images/img05.jpg" alt="">
      </div>
    </section>
  </div><!-- //l_wrapper -->
</body>
</html>
