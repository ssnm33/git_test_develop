$(function() {
  $('.content').css('visibility','hidden');
  $(window).scroll(function(){
    var windowH = $(window).height(),
        topWindow = $(window).scrollTop();
    $('.content').each(function(){
      var targetPosition = $(this).offset().top;
      if(topWindow > targetPosition - windowH + 200){
        $(this).addClass('fadeInDown');
      }
    })
  });
});
$(document).ready(function(){
  var e=$(window).height();
  $("#preloader").height(e).css("display","block");
  $("#preloader").css("lineHeight",e + "px");
})
$(window).load(function(){
  $("#preloader").delay(1500).fadeOut(750),
  $("body").css({overflow:"visible"})
})
